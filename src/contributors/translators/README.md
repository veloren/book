# Translation working group

_Introduction on translating content to your language._

- [Translating the game](/contributors/translators/game-translation.md)
- [Managing Weblate](/contributors/translators/weblate-admin.md)
